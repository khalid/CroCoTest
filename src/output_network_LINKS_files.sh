### OUTPUT : network files - step 3 ("LINKS.csv")
#echo -e "\tLINKS_gephi.csv\n\tLINKS_diagrammer.tsv\n\tLINKS_gephi_dubious.csv\n\tLINKS_diagrammer_dubious.tsv"

echo -e "Source,Target,Type,Weight" > LINKS_gephi.csv
echo -e "from\tto\trel\tcontamination" > LINKS_diagrammer.tsv
echo -e "Source,Target,Type,Weight" > LINKS_gephi_dubious.csv
echo -e "from\tto\trel\tcontamination" > LINKS_diagrammer_dubious.tsv

for f in *.all ; do
	sp1=`basename $f .all`
	for f in *.all ; do
		sp2=`basename $f .all`
		if [ "$sp1" != "$sp2" ] ; then
			weight=`grep -c "$sp1,$sp2" LINKS_exhaustive.csv`
			echo -e "$sp1,$sp2,Directed,$weight" >> LINKS_gephi.csv
			echo -e "$sp1\t$sp2\tcontaminate\t$weight" >> LINKS_diagrammer.tsv
			weight_dubious=`grep -c "$sp1,$sp2" LINKS_exhaustive_dubious.csv`
			echo -e "$sp1,$sp2,Directed,$weight_dubious" >> LINKS_gephi_dubious.csv
			echo -e "$sp1\t$sp2\tcontaminate\t$weight_dubious" >> LINKS_diagrammer_dubious.tsv
		fi
	done
done
