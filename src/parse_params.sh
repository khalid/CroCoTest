function printUsage(){
echo -e "\n`basename $0` is a program that can detect potential cross-contamination in assembled transcriptomes using sequencing reads to find true origin of transcripts.

Usage :
$0 [--cnf configFile] [--mode p|u] [--tool B|B2|K|R|S] [--fold-threshold INT] [--minimum-coverage FLOAT] [--threads INT] [--output-prefix STR] [--output-level 1|2|3] [--graph yes|no] [--trim5 INT] [--trim3 INT] [--frag-length FLOAT] [--frag-sd FLOAT] [--suspect-id INT] [--suspect-len INT] [--add-option STR] [--recat STR] [--readclean yes|no]

--cnf configFile : a text filename containg a list of transcriptome assemblies to analyze and their associated fastq reads files [short: -k]
--mode p|u :\t\t\t'p' for paired and 'u' for unpaired (default : 'p') [short: -m]
--in STR :\t\t\tName of the directory containing the input files to be analyzed (DEFAULT : working directory) [short: -i]
--tool B|K|R :\t\t'B' for bowtie, 'K' for kallisto, 'R' for rapmap (DEFAULT : 'R') [short: -t]
--fold-threshold FLOAT :\tValue between 1 and N (DEFAULT : 2) [short: -f]
--minimum-coverage FLOAT :\tTPM value (DEFAULT : 0.2) [short: -c]
--overexp FLOAT :\t\t\tTPM value (DEFAULT : 300) [short: -d]
--threads INT :\t\t\tNumber of threads to use (DEFAULT : 1) [short: -n]
--output-prefix STR :\t\tPrefix of output directory that will be created (DEFAULT : empty) [short: -p]
--output-level 1|2 :\t\tSelect whether or not to output fasta files. '1' for none, '2' for all (DEFAULT : 2) [short: -l]
--graph yes|no :\t\tProduce graphical output using R (DEFAULT : no) [short: -g]
--readclean yes|no :\t\tSelect whether or not to output fastq files devoid of reads that mapped onto contaminant transcripts (DEFAULT : no) [short: -z]
--add-option 'STR' :\t\tThis text string will be understood as additional options for the mapper/quantifier used (DEFAULT : empty) [short: -a]
--recat SRT :\t\t\tName of a previous CroCo output directory you wish to use to re-categorize transcripts (DEFAULT : no) [short: -r]
--trim5 INT :\t\t\tnb bases trimmed from 5' (DEFAULT : 0) [short: -x]
--trim3 INT :\t\t\tnb bases trimmed from 3' (DEFAULT : 0) [short: -y]
--suspect-id INT :\t\tIndicate the minimum percent identity between two transcripts to suspect a cross contamination (DEFAULT : 95) [short: -s]
--suspect-len INT :\t\tIndicate the minimum length of an alignment between two transcripts to suspect a cross contamination (DEFAULT : 40) [short: -w]
--frag-length FLOAT :\t\tEstimated average fragment length (no default value). Only used in specific combinations of --mode and --tool  [short: -u]
--frag-sd FLOAT :\t\tEstimated standard deviation of fragment length (no default value). Only used in specific combinations of --mode and --tool [short: -v]

It is good practice to redirect information about each CroCo run into an output log file using the following structure :
'2>&1 | tee log_file'

Minimal working example :
CroCo_v0.1.sh --cnf sampleconfig.txt --mode p 2>&1 | tee log_file

Exhaustive example :
CroCo_v0.1.sh --cnf configFile --mode p --in data_folder_name --tool K --fold-threshold 2 --minimum-coverage 0.2 --overexp 300 --threads 8 --output-prefix test1_ --output-level 2 --graph yes --add-option '-v 0' --trim5 0 --trim3 0 --suspect-id 95 --suspect-len 40 --recat no --readclean no 2>&1 | tee log_file

Exhaustive example using shortcuts :
CroCo_v0.1.sh -k configFile -m p -i data_folder_name -t K -f 2 -c 0.2 -d 300 -n 8 -p test1_ -l 2 -g yes -a '-v 0' -x 0 -y 0 -s 95 -w 40 -r no -z no 2>&1 | tee log_file

Example for re-categorizing previous CroCo results
CroCo_v0.1.sh -k configFile -i data_folder_name -r previous_CroCo_results_folder_name -f 10 -c 0.5 -g yes 2>&1 | tee log_file

"
}

function printAndUsageAndExit(){
    echo
    echo "!!! Fatal error !!!"
    echo $1
    echo
    echo "-----------------------------------------------------------------------------------------"
    printUsage
    exit 1
}

number_re='^[0-9]+$'
float_re='^[0-9]+([.][0-9]+)?$'

ARGS=$(getopt -o k:m:i:f:x:y:c:t:n:p:l:g:a:u:v:s:r:w:d:z: --long cnf:,mode:,in:,fold-threshold:,trim5:,trim3:,minimum-coverage:,tool:,threads:,output-prefix:,output-level:,graph:,add-option:,frag-length:,frag-sd:,suspect-id:,recat:,suspect-len:,overexp:,readclean: -n "$0" -- "$@");

#Bad arguments
if [ $? -ne 0 ] || [ $# -eq 0 ];
then
    printUsage
    exit
fi
MODEFLAG=0
CONFFLAG=0
eval set -- "$ARGS";

while true; do
    case "$1" in
        -a|--add-option)
            shift;
            if [ -n "$1" ]; then
                ADDOPT=$1
            else
                printAndUsageAndExit "You have to set a non-empty value for option --add-option if you decide to use it"
            fi
            shift;
            ;;
        -c|--minimum-coverage)
            shift;
            if [ -n "$1" ]; then
                if ! [[ $1 =~ $float_re ]] ; then
                    printAndUsageAndExit "Value for option --minimum-coverage must be float ('$1' was given)"
                else
                    MINCOV=$1
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --minimum-coverage"
            fi
            shift;
            ;;
        -d|--overexp)
            shift;
            if [ -n "$1" ]; then
                if ! [[ $1 =~ $float_re ]] ; then
                    printAndUsageAndExit "Value for option --overexp must be float ('$1' was given)"
                else
                    OVEREXP=$1
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --overexp"
            fi
            shift;
            ;;
        -f|--fold-threshold)
            shift;
            if [ -n "$1" ]; then
                if ! [[ $1 =~ $float_re ]] ; then
                    printAndUsageAndExit "Value for option --fold-threshold must be float ('$1' was given)"
                else
                    FOLD=$1
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --fold-threshold"
            fi
            shift;
            ;;
        -g|--graph)
            shift;
            if [ -n "$1" ]; then
                if [[ "$1" == "yes" ]] || [[ "$1" == "no" ]]; then
                    GRAPH=$1
                else
                    printAndUsageAndExit "'$1' is an incorrect value for --graph option (yes/no accepted)"
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --graph"
            fi
            shift;
            ;;
        -i|--in)
            shift;
            if [ -n "$1" ]; then
                opt=$1
                tmp_indir=$(readlink -f "$opt")
                INDIR=${tmp_indir/ /_}
            else
                printAndUsageAndExit "You have to set a non-empty value for option --in if you decide to use it"
            fi
            shift;
            ;;
        -k|--cnf)
            shift;
            if [ -n "$1" ]; then
               CONFFLAG=1
               CONFFILE=$1
            else
                printAndUsageAndExit "You have to set a non-empty value for option --cnf to list the files to be analyzed"   
            fi
            shift;
            ;;
        -l|--output-level)
            shift;
            if [ -n "$1" ]; then
                if [[ "$1" == "1" ]] || [[ "$1" == "2" ]] || [[ "$1" == "3" ]]; then
                    OUTPUTLEVEL=$1
                else
                    printAndUsageAndExit "'$1' is an incorrect value for --output-level option (1, 2 or 3 accepted)"
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --output-level"
            fi
            shift;
            ;;
        -m|--mode)
            shift;
            if [ -n "$1" ]; then
                if [[ "$1" == "p" ]] || [[ "$1" == "u" ]]; then
                    MODEFLAG=1
                    MODE=$1
                else
                    printAndUsageAndExit "'$1' is an incorrect value for --mode option"
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --mode"
            fi
            shift;
            ;;
        -n|--threads)
            shift;
            if [ -n "$1" ]; then
                if ! [[ $1 =~ $number_re ]] ; then
                    printAndUsageAndExit "Value for option --threads must be numeric ($1 was given)"
                else
                    PROCESSORS=$1
                    if ((PROCESSORS < 1)); then
                        printAndUsageAndExit "Value for option --threads must be positive ($1 was given)"
                    fi
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --threads"
            fi
            shift;
            ;;
        -p|--output-prefix)
            shift;
            if [ -n "$1" ]; then
                opt=$1
                OUTPUTPREFIX=${opt/ /_}
            else
                printAndUsageAndExit "You have to set a non-empty value for option --output-prefix"
            fi
            shift;
            ;;
        -r|--recat)
            shift;
            if [ -n "$1" ]; then
                RECAT=$1
            else
                printAndUsageAndExit "You have to set a non-empty value for option --recat if you decide to use it"
            fi
            shift;
            ;;
        -s|--suspect-id)
            shift;
            if [ -n "$1" ]; then
                if ! [[ $1 =~ $float_re ]] ; then
                    printAndUsageAndExit "Value for option --suspect-id must be float ('$1' was given)"
                else
                    SUSPID=$1
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --suspect-id"
            fi
            shift;
            ;;
        -t|--tool)
            shift;
            if [ -n "$1" ]; then
                if [[ "$1" == "B" ]] || [[ "$1" == "B2" ]] || [[ "$1" == "K" ]] || [[ "$1" == "R" ]] || [[ "$1" == "S" ]] || [[ "$1" == "H" ]]; then
                    TOOL=$1
                    case "$TOOL" in
                        B) which bowtie > /dev/null; ret=$?; if ((ret!=0)); then printAndUsageAndExit "Could not find bowtie in utils/bin/bowtie-1.1.2/ or in PATH"  ;fi   ;;
                        K) which kallisto > /dev/null; ret=$?; if ((ret!=0)); then printAndUsageAndExit "Could not find kallisto in utils/bin/kallisto/ or in PATH"  ;fi   ;;
                        R) which rapmap > /dev/null; ret=$?; if ((ret!=0)); then printAndUsageAndExit "Could not find rapmap in utils/bin/rapmap/bin/ or in PATH"  ;fi   ;;
                    esac
                else
                    printAndUsageAndExit "'$1' is an incorrect value for --tool option (B, K and R accepted)"
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --tool"
            fi
            shift;
            ;;
        -u|--frag-length)
            shift;
            if [ -n "$1" ]; then
				if ! [[ $1 =~ $float_re ]] ; then
                    printAndUsageAndExit "Value for option --frag-length must be float ('$1' was given)"
                else
                    FRAGLENGTH=$1
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --frag-length"
            fi
            shift;
            ;;
        -v|--frag-sd)
            shift;
            if [ -n "$1" ]; then
				if ! [[ $1 =~ $float_re ]] ; then
                    printAndUsageAndExit "Value for option --frag-sd must be float ('$1' was given)"
                else
                    FRAGSD=$1
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --frag-sd"
            fi
            shift;
            ;;
        -w|--suspect-len)
            shift;
            if [ -n "$1" ]; then
                if ! [[ $1 =~ $number_re ]] ; then
                    printAndUsageAndExit "Value for option --suspect-id must be numeric ('$1' was given)"
                else
                    SUSPLEN=$1
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --suspect-id"
            fi
            shift;
            ;;
        -x|--trim5)
            shift;
            if [ -n "$1" ]; then
                if ! [[ $1 =~ $number_re ]] ; then
                    printAndUsageAndExit "Value for option --trim5 must be a positive integer ('$1' was given)"
                else
                    TRIM5=$1
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --trim5"
            fi
            shift;
            ;;
        -y|--trim3)
            shift;
            if [ -n "$1" ]; then
                if ! [[ $1 =~ $number_re ]] ; then
                    printAndUsageAndExit "Value for option --trim3 must be a positive integer ($1 was given)"
                else
                    TRIM3=$1
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --trim3"
            fi
            shift;
            ;;
        -z|--readclean)
            shift;
            if [ -n "$1" ]; then
                CLEANING=$1
            else
                printAndUsageAndExit "You have to set a non-empty value for option --readclean if you decide to use it"
            fi
            shift;
            ;;
        --)
            break;
            ;;
    esac
done

if (( MODEFLAG == 0 )) || (( CONFFLAG == 0 )) ; then
	if [ $RECAT == "no" ]; then
	    printAndUsageAndExit "You must set both the --mode and the --conf options"
	fi
fi

# alert for particular combinations of tool and mode
if [[ $MODE == "u" ]] && [[ $RECAT != "no" ]]; then
 if [ $TOOL == "S" ] || [ $TOOL == "K" ]; then
  if [ $FRAGLENGTH == "" ] || [ $FRAGSD == "" ]; then
   echo -e "\nWARNING:\nUsing the $MODE mode and the $TOOL tool, you need to specify values for frag-length and frag-sd options.\n"
   printAndUsageAndExit
  fi
 fi
fi
