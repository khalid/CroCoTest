### OUTPUT : network files - step 5 (various 'nodes' files)
#echo -e "\tNODES_gephi_absolute.csv\n\tNODES_gephi_norm.csv\n\tNODES_diagrammer.tsv\n\tNODES_gephi_dubious_absolute.csv\n\tNODES_gephi_dubious_norm.csv\n\tNODES_diagrammer_dubious.tsv"

echo -e "Id,Label,NB_contaminant,NB_contaminated" > NODES_gephi_absolute.csv
echo -e "Id,Label,NB_contaminant,NB_norm_contaminated" > NODES_gephi_norm.csv
echo -e "nodes\tlabel\tNB_contaminant\tNB_contaminated" > NODES_diagrammer.tsv
echo -e "Id,Label,NB_contaminant,NB_contaminated" > NODES_gephi_dubious_absolute.csv
echo -e "Id,Label,NB_contaminant,NB_norm_contaminated" > NODES_gephi_dubious_norm.csv
echo -e "nodes\tlabel\tNB_contaminant\tNB_contaminated" > NODES_diagrammer_dubious.tsv

for f in *.all ; do
	sp=`basename $f .all`
	nb_tot_seq=`grep \> ../$sp.fasta | wc -l`

	nb_contaminant=`grep ^$sp nodes_detailed.csv | grep -v $sp$ | cut -f2 | awk '{print $NF;}' | awk '{SUM += $1} END {print SUM}'`
	nb_contaminee=`grep "total contaminations $sp" nodes_detailed.csv | awk '{print $NF;}'`
	norm_contaminant=`echo "scale=2; $nb_contaminant * 100 / $nb_tot_seq" | bc -l | awk '{printf "%.2f", $0}'`
	norm_contaminee=`echo "scale=2; $nb_contaminee * 100 / $nb_tot_seq" | bc -l | awk '{printf "%.2f", $0}'`
	echo -e $sp","$sp","$nb_contaminant","$nb_contaminee >> NODES_gephi_absolute.csv
	echo -e $sp","$sp","$nb_contaminant","$norm_contaminee >> NODES_gephi_norm.csv
	echo -e $sp"\t"$sp"\t"$nb_contaminant"\t"$nb_contaminee >> NODES_diagrammer.tsv

	nb_contaminant_dubious=`grep ^$sp nodes_detailed_dubious.csv | grep -v $sp$ | cut -f2 | awk '{print $NF;}' | awk '{SUM += $1} END {print SUM}'`
	nb_contaminee_dubious=`grep "total contaminations $sp" nodes_detailed_dubious.csv | awk '{print $NF;}'`
	norm_contaminant_dubious=`echo "scale=2; $nb_contaminant_dubious * 100 / $nb_tot_seq" | bc -l | awk '{printf "%.2f", $0}'`
	norm_contaminee_dubious=`echo "scale=2; $nb_contaminee_dubious * 100 / $nb_tot_seq" | bc -l | awk '{printf "%.2f", $0}'`
	echo -e $sp","$sp","$nb_contaminant_dubious","$nb_contaminee_dubious >> NODES_gephi_dubious_absolute.csv
	echo -e $sp","$sp","$nb_contaminant_dubious","$norm_contaminee_dubious >> NODES_gephi_dubious_norm.csv
	echo -e $sp"\t"$sp"\t"$nb_contaminant_dubious"\t"$nb_contaminee_dubious >> NODES_diagrammer_dubious.tsv
done

