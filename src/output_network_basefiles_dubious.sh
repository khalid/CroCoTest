### OUTPUT : network files ("LINKS_exhaustive_dubious.csv" and "nodes_detailed_dubious.csv" -- using only dubious)
cd $out
echo -e "\tLINKS files (dubious)\n\tNODES files (dubious)"
echo -e "Source,Target,Type,Weight" > LINKS_exhaustive_dubious.csv
for sortie in *.all ; do
 #echo -e "$sortie"
 infile=`basename $sortie .all`
 awk -v INFILE=$infile 'BEGIN {currentsp=INFILE}
{ colMax= NF -2
if(NR==1) {
	for (i=2; i < NF - 2; i++) {
		sub("_reads","", $i)
		taxons[i] = $i
		tax = taxons[i]
		comptage[tax]=0
	}
}
else {
  	max = $colMax
	if ($NF == "dubious") {
		for (i=2; i < NF - 2; i++) if ($i == max) {tax = taxons[i]; break}
			if (taxons[i] != currentsp) {
				print tax","currentsp",Directed,1.0" >> "LINKS_exhaustive_dubious.csv"
				comptage[tax]++
				comptage[currentsp]++
			}
	}
}
}
END{ for (i in comptage) {
		print i"\tnb_contaminants = "comptage[i]"\tsp_contaminated = "currentsp >> "nodes_detailed_dubious.csv"
	}
	print "total contaminations "currentsp" = "comptage[currentsp]"\n" >> "nodes_detailed_dubious.csv"
}
 ' $sortie 2>/dev/null
done

