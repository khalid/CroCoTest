############################ PATH MANAGEMENT ############################
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
crossScriptDir=$(readlink -f "$0")
crosscontamdir=`dirname $crossScriptDir`
crosscontamtopdir=`dirname $crosscontamdir`
bowtie_path="${crosscontamtopdir}/utils/bin/bowtie-1.1.2"
kallisto_path="${crosscontamtopdir}/utils/bin/kallisto/"
RapMap_path="${crosscontamtopdir}/utils/bin/rapmap/bin/"
BLAST_path="${crosscontamtopdir}/utils/bin/ncbi-blast-2.5.0+/bin"
CrocoBLAST_path="${crosscontamtopdir}/utils/bin"
export PATH=$bowtie_path:$kallisto_path:$RapMap_path:$BLAST_path:$CrocoBLAST_path:$PATH
