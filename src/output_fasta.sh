### OUTPUT : fasta files sorted by categories (clean, contam, dubious, lowcov, overexp)
if [ $OUTPUTLEVEL == "1" ]; then
	echo -e "\nfasta files will not be written (output level set to '1')"

elif [ $OUTPUTLEVEL == "2" ]; then

	echo ""; echo "writing categorized transcriptomes (fasta files)"
	for ref in "${orders[@]}"; do
		InfosCtg=${fasta_array[$ref]}
		fasta="$INDIR"/"`echo $InfosCtg | cut -d';' -f1`"
		echo -e "\t$ref"

		# insuring one-line fasta sequence format
		awk -v ref=$ref -v out=$out  'BEGIN{RS=">"; FS="\t"} NR>1 { sub("\n","\t"); gsub("\n","",$0);
		print ">"$1"\n"$2 > out"/"ref".fasta.oneline"}' $fasta

		# writing problematic categories (excluding unsuspected transcripts)
		for suff in lowcov contam dubious overexp; do
			if [ ! -f "$out/$ref.$suff" ]; then touch $out/$ref.$suff; fi
			cat $out/$ref.$suff | cut -f1 | grep -w -F -f $out/$ref.suspects | sed "s/$ref|//g" > $out/$ref.$suff.list
			cat $out/$ref.$suff.list >> $out/$ref.allunclean.list
			cat $out/$ref.fasta.oneline | paste - - | grep -w -F -f $out/$ref.$suff.list | tr "\t" "\n" > $out/$ref\_$suff.fasta
		done

		# writing clean category (including unsuspected transcripts)
		cat $out/All_transcripts.quants | cut -f1 | grep "$ref|" | sed "s/$ref|//g" > $out/$ref.alltranscripts.list
		grep -v -w -F -f $out/$ref.allunclean.list $out/$ref.alltranscripts.list > $out/$ref.allclean.list
		cat $out/$ref.fasta.oneline | paste - - | grep -w -F -f $out/$ref.allclean.list | tr "\t" "\n" > $out/$ref\_clean.fasta
	done

	# miscellaneous
	mv $out/*.list $out/*.fasta.oneline $out/utility_files_CroCo

else echo -e "\nwarning : output level value must be set to either '1' or '2' (default = '2')"
fi




	#echo -e "\nWriting categorized transcriptomes - all categories (it might take some time)"
	#for fasta in $out/*.fasta_mod; do
		#ref=`basename $fasta .fasta_mod`
	#for ref in "${orders[@]}"; do
	#	fasta=$out/$ref".fasta_mod"
	#	echo -e "\t$ref"

		# version awk
	#	awk -v ref=$ref -v out=$out  'BEGIN{RS=">"; FS="\t"} NR>1 {
	#	sub("\n","\t");
	#	gsub("\n","",$0);
	#	ident =split($1,a," ")              # N.B the seq idents have been changed in line 37 (supr all text after the first space)
	#	print a[1]"\t"$2 > out"/"ref".togrep"
		#grpcmd = "LC_ALL=C fgrep -w -c -m 1 \""a[1] "\" "
		#go="_clean" # default state is clean
		#if (system(" [ -f " out"/"ref".lowcov ]") == 0) { cmd = grpcmd out"/"ref".lowcov"; cmd |& getline ret; close(cmd); if ( ret != 0 ) { go="_lowcov" } }
		#print RS$1"\n"$2 > out"/"ref""go".fasta"
	#	}' $fasta

		# tentative de sortie du awk pour acceleration
	#	for suff in lowcov contam dubious overexp; do
	#		if [ ! -f "$out/$ref.$suff" ]; then echo "" > $out/$ref.$suff; fi
	#	done
	#	cat $out/$ref".togrep" | while read line; do
	#		ctg=`echo $line | cut -d' ' -f1`
	#		go="_clean"
	#		if LC_ALL=C grep -F -q -w -m1 "$ctg" $out/$ref.all; then			# if contig is suspect
	#			if LC_ALL=C grep -F -q -w -m1 "$ctg" $out/$ref.lowcov ; then go="_lowcov"
	#			elif LC_ALL=C grep -F -q -w -m1 "$ctg" $out/$ref.contam ; then go="_contam"
	#			elif LC_ALL=C grep -F -q -w -m1 "$ctg" $out/$ref.dubious ; then go="_dubious" 
	#			elif LC_ALL=C grep -F -q -w -m1 "$ctg" $out/$ref.overexp ; then go="_overexp" 
	#			fi
	#		fi
	#	echo ">"$line >> $out"/"$ref""$go".fasta"
	#	done
	#	for f in $out/$ref\_*.fasta; do
	#		sed -i 's/ /\n/g' $f
	#	done
		# fin de tentative

	#	if [ -f $out/$ref"_clean.fasta" ]; then sed -i "s/$ref|//g" $out/$ref"_clean.fasta"; fi
	#	if [ -f $out/$ref"_lowcov.fasta" ]; then sed -i "s/$ref|//g" $out/$ref"_lowcov.fasta"; fi
	#	if [ -f $out/$ref"_dubious.fasta" ]; then sed -i "s/$ref|//g" $out/$ref"_dubious.fasta"; fi
	#	if [ -f $out/$ref"_overexp.fasta" ]; then sed -i "s/$ref|//g" $out/$ref"_overexp.fasta"; fi
	#done



