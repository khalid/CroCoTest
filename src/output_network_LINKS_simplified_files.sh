### OUTPUT : network files - step 4 ("LINKS_gephi_simplified.csv", "LINKS_diagrammer_simplified.tsv")
#echo -e "\tLINKS_gephi_simplified.csv\n\tLINKS_diagrammer_simplified.tsv"

head -n1 LINKS_gephi.csv > LINKS_gephi_simplified.csv
head -n1 LINKS_diagrammer.tsv > LINKS_diagrammer_simplified.tsv
max_link=`cat LINKS_gephi.csv | sed '1d' | cut -d "," -f4 | awk '$0>x{x=$0};END{print x}'`
division_factor=50
if [[ "$max_link" -eq "0" ]]; then threshold=0
else threshold=$(($max_link/$division_factor)); fi

echo -e "Simplifying cross contamination network : removal of links below or equal to "$threshold" cross contamination events"
cat LINKS_gephi.csv | grep -v 'Weight' | while read line ; do
	current_link=`echo $line | cut -d "," -f4`
	if [ "$current_link" -gt "$threshold" ] ; then
		echo $line >> LINKS_gephi_simplified.csv
	fi
done

cat LINKS_diagrammer.tsv | grep -v 'contamination' | while read line2 ; do
	current_link=`echo $line2 | cut -d' ' -f4`
	if [[ "$current_link" -gt "$threshold" ]]; then
		echo $line2 >> LINKS_diagrammer_simplified.tsv
	fi
done

#echo -e "\tLINKS_gephi_dubious_simplified.csv\n\tLINKS_diagrammer_dubious_simplified.tsv"

head -n1 LINKS_gephi_dubious.csv > LINKS_gephi_dubious_simplified.csv
head -n1 LINKS_diagrammer_dubious.tsv > LINKS_diagrammer_dubious_simplified.tsv
max_link=`cat LINKS_gephi_dubious.csv | sed '1d' | cut -d "," -f4 | awk '$0>x{x=$0};END{print x}'`
division_factor=50
if [[ "$max_link" -eq "0" ]]; then threshold=0
else threshold=$(($max_link/$division_factor)); fi

echo -e "Simplifying dubious cross contamination network : removal of links below or equal to "$threshold" dubious cross contamination events"
cat LINKS_gephi_dubious.csv | grep -v 'Weight' | while read line ; do
	current_link=`echo $line | cut -d "," -f4`
	if [ "$current_link" -gt "$threshold" ] ; then
		echo $line >> LINKS_gephi_dubious_simplified.csv
	fi
done

cat LINKS_diagrammer_dubious.tsv | grep -v 'contamination' | while read line2 ; do
	current_link=`echo $line2 | cut -d' ' -f4`
	if [[ "$current_link" -gt "$threshold" ]]; then
		echo $line2 >> LINKS_diagrammer_dubious_simplified.tsv
	fi
done


